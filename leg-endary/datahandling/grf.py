# leg-firmware/docs/datahandling/GRF.md

import pandas as pd

def get_GRF(grf, gait_cycle, type):

	# Reads file in using Pandas
	file = "../../data/groundreactionforces.csv"
	df = pd.read_csv(file)

	# Calculate condition positions
	index = df.index
	condition = (df["Gait Cycle"] == gait_cycle) & (df["GRF"] == grf)
	condition_index = index[condition].tolist()

	# Trys to return value at given conditions
	try:
		return df[type][condition_index].values[0]
	except:
		return None