import hashlib
import datetime
import os

def __generate_instance_id():
	now = str(datetime.datetime.now()).encode()
	return hashlib.sha1(now).hexdigest()[:10]


def new_training_data_instance(speed):
	instance_id = __generate_instance_id()
	path = f"../../data/training-data/{instance_id}/"

	os.mkdir(path)

	for x in range(0, 8):
		with open(f"{path}s{str(x)}", "w+") as f:
			f.write(f"INSTANCE ID: {instance_id} SENSOR: s{str(x)} VELOCITY: {str(speed)}\n")


def new_single_data_instance(speed, calibration):
	instance_id = __generate_instance_id()
	path = f"../data/training-data/{instance_id}/"

	os.mkdir(path)

	with open(f"{path}calibration.csv", "w") as f:
		f.write(f"{calibration[0]},{calibration[1]},{calibration[2]}")
	with open(f"{path}s0.csv", "w") as f:
		f.write(f"INSTANCE ID: {instance_id} SENSOR: t0 VELOCITY: {str(speed)}\n")
	return f"{path}/s0.csv"


def append_sensor_data(path, data):
	with open(path, "a") as f:
		f.write(f"{data[0]},{data[1]},{data[2]}\n")

# new_single_data_instance("x", [1, 1, 1])