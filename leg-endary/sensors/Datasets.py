class Vector3:
	
	x = 0
	y = 0
	z = 0

	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

	# Setter
	def set(x, y, z):
		self.x = x
		self.y = y
		self.z = z

	# Getter
	def get():
		return self.x,self.y,self.z

	# Update Division Operator
	def __truediv__(self, other):
		if (type(other) == Vector3):
			return Dataset(self.x / other.x, self.y / other.y, self.z / other.z)
		else:
			return Dataset(self.x / other, self.y / other, self.z / other)

	# Updates Addition Operator
	def __add__(self, other):
		if (type(other) == Vector3):
			return Dataset(self.x + other.x, self.y + other.y, self.z + other.z)
		else:
			return Dataset(self.x + other, self.y + other, self.z + other)